bl_info = {
    "name": "Bone Widget",
    "author": "Christophe SEUX",
    "version": (2, 0, 0),
    "blender": (2, 92, 0),
    "description": "Create custom shapes for bone controller",
    "warning": "",
    "wiki_url": "",
    "category": "Rigging",
}

import sys


if "bpy" in locals():
    import importlib as imp
    imp.reload(context)
    imp.reload(properties)
    imp.reload(operators)
    imp.reload(ui)
else:
    from . import context
    from . import operators
    from . import ui
    from . import properties

import bpy

#sys.modules.update({"bone_widget.ctx": context.BW_ctx()})
from bone_widget import ctx


def register():
    properties.register()
    operators.register()
    ui.register()
    
    #bpy.types.Scene.bone_widget = bpy.props.PointerProperty(type=BoneWidgetSettings)
    #get_widgets(DefaultFolder, DefaultShapes)
    #get_widgets(CustomFolder, CustomShapes)
    for f in ctx.folders:
        f.load_widgets()


def unregister():
    #print('UnRegister BoneWidget')
    
    properties.unregister()
    operators.unregister()
    ui.unregister()

    del sys.modules['bone_widget.ctx']

    #remove_icons(bpy.types.Scene.bone_widget)

